package com.twuc.webApp;

import java.util.HashMap;
import java.util.Map;

public class SlotStorage {
    private SlotSize slotSize;
    private int capacity;
    private int remain;
    private Map<Ticket, Bag> bags = new HashMap<>();

    public SlotStorage(SlotSize slotSize, int capacity) {
        this.slotSize = slotSize;
        this.capacity = capacity;
        this.remain = capacity;
    }

    public Bag retrieve(Ticket ticket) {
        if(this.bags.size()==0) {
            return null;
        }
        if (!this.bags.containsKey(ticket)) {
            throw new NullPointerException("Invalid Ticket");
        }
        remain++;
        return bags.remove(ticket);
    }

    public Ticket save(Bag bag) {
        Ticket ticket = new Ticket();
        try {
            String notMatch = "Cannot save your bag: "+bag.getSize();
            switch (bag.getSize()) {
                case BIG:
                    if (this.slotSize != SlotSize.BIG) {
                        throw new IllegalArgumentException(notMatch+" "+this.slotSize);
                    }
                    break;
                case MEDIUM:
                    if (this.slotSize == SlotSize.SMALL) {
                        throw new IllegalArgumentException(notMatch+" "+this.slotSize);
                    }
                    break;
                case SMALL:
                    break;
            }
            bags.put(ticket, bag);
            remain--;
            if (remain < 0) {
                throw new IndexOutOfBoundsException("Insufficient capacity");
            }
        } catch (NullPointerException e) {
            return ticket;
        }
        return ticket;
    }

    public SlotSize getSlotSize() {
        return slotSize;
    }
}
