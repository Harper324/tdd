package com.twuc.webApp;

public enum BagSize {
    BIG,
    MEDIUM,
    SMALL
}
