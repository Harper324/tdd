package com.twuc.webApp;

public class Bag {
    private BagSize bagSize=BagSize.SMALL;

    public Bag() {
    }

    public Bag(BagSize bagSize) {
        this.bagSize = bagSize;
    }

    public BagSize getSize() {
        return bagSize;
    }
}
