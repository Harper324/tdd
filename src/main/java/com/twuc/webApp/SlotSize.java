package com.twuc.webApp;

public enum SlotSize {
    BIG,
    MEDIUM,
    SMALL
}
