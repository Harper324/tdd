package com.twuc.webApp;

import java.util.HashMap;
import java.util.Map;

public class Storage {

    private final Map<SlotSize, SlotStorage> slotMap = new HashMap<>();
    private SlotSize slotSize;

    public Storage(int smallSlotCapacity) {
        this(smallSlotCapacity, 0, 0);
    }

    public Storage(int smallSlotCapacity, int mediumSlotCapacity, int bigSlotCapacity) {
        SlotStorage smallSlots = new SlotStorage(SlotSize.SMALL, smallSlotCapacity);
        SlotStorage mediumSlots = new SlotStorage(SlotSize.MEDIUM, mediumSlotCapacity);
        SlotStorage bigSlots = new SlotStorage(SlotSize.BIG, bigSlotCapacity);
        slotMap.put(SlotSize.SMALL, smallSlots);
        slotMap.put(SlotSize.MEDIUM, mediumSlots);
        slotMap.put(SlotSize.BIG, bigSlots);
    }

    public Ticket save(Bag bag) {
        return save(bag, SlotSize.SMALL);
    }

    public Ticket save(Bag bag, SlotSize slotSize) {
        this.slotSize = slotSize;
        return slotMap.get(slotSize).save(bag);
    }

    public Bag retriev(Ticket ticket) {
        return slotMap.get(slotSize).retrieve(ticket);
    }
}
