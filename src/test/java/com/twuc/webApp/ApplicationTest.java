package com.twuc.webApp;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ApplicationTest {

    private Storage getEnoughStorage() {
        return new Storage(20);
    }

    @Test
    void should_return_ticket() {
        Storage storage = getEnoughStorage();
        Bag bag = new Bag();
        Ticket ticket = storage.save(bag);
        assertNotNull(ticket);
    }

    @Test
    void should_return_ticket_when_saving_nothing() {
        Storage storage = getEnoughStorage();
        Ticket ticket = storage.save(null);
        assertNotNull(ticket);
    }

    @Test
    void should_return_bag_when_giving_valid_and_ticket_not_been_uesd() {
        Storage storage = getEnoughStorage();
        Bag actualBag = new Bag();
        Ticket ticket = storage.save(actualBag);
        Object expectedBag = storage.retriev(ticket);
        assertSame(actualBag, expectedBag);
    }

    @Test
    void should_return_error_giving_invalid_ticket() {
        Storage storage = getEnoughStorage();
        Bag actualBag = new Bag();
        storage.save(actualBag);
        Ticket invalidTicket = new Ticket();
        NullPointerException exception = assertThrows(
                NullPointerException.class, () ->
                        storage.retriev(invalidTicket)
        );
        assertEquals("Invalid Ticket", exception.getMessage());
    }

    @Test
    void should_return_nothing_when_saving_nothing_and_giving_ticket() {
        Storage storage = getEnoughStorage();
        Ticket ticket = storage.save(null);
        Bag noBag = storage.retriev(ticket);
        assertNull(noBag);
    }

    @Test
    void should_get_ticket_when_storage_not_full_and_save_bag() {
        Storage storage = new Storage(2);
        Bag bag1 = new Bag();
        Bag bag2 = new Bag();
        storage.save(bag1);
        Ticket ticket = storage.save(bag2);
        assertNotNull(ticket);
    }

    @Test
    void should_get_insufficient_message_when_save() {
        Storage storage = new Storage(2);
        storage.save(new Bag());
        storage.save(new Bag());
        IndexOutOfBoundsException exception = assertThrows(
                IndexOutOfBoundsException.class, () ->
                        storage.save(new Bag())
        );
        assertEquals("Insufficient capacity", exception.getMessage());
    }

    @Test
    void should_save_first_bag_and_return_message_when_storage_is_1() {
        Storage storage = new Storage(1);
        Ticket ticket = storage.save(new Bag());
        assertNotNull(ticket);
        IndexOutOfBoundsException exception = assertThrows(
                IndexOutOfBoundsException.class, () ->
                        storage.save(new Bag())
        );
        assertEquals("Insufficient capacity", exception.getMessage());
    }

    @Test
    void should_save_another_bag_when_storage_is_full_and_retrieve_1() {
        Storage storage = new Storage(1);
        Bag bag = new Bag();
        Ticket ticket = storage.save(bag);
        storage.retriev(ticket);
        Bag anotherBag = new Bag();
        Ticket anotherTicket = storage.save(anotherBag);
        assertNotNull(anotherTicket);
    }

    @Test
    void should_save_small_bag_when_big_slot_is_1() {
        Storage storage = new Storage(1, 0, 0);
        Bag bag = new Bag();
        Ticket ticket = storage.save(bag);
        assertNotNull(ticket);
    }

    @Test
    void should_return_message_when_save_big_bag_to_small_slot() {
        Storage storage = new Storage(1, 1, 1);
        Bag bag = new Bag(BagSize.BIG);
        IllegalArgumentException exception = assertThrows(
                IllegalArgumentException.class, () ->
                        storage.save(bag, SlotSize.SMALL)
        );
        assertEquals("Cannot save your bag: BIG SMALL", exception.getMessage());
    }

    @Test
    void should_return_message_when_big_slot_is_full_and_save_medium_bag_to_it() {
        Storage storage = new Storage(0, 1, 0);
        Bag bag = new Bag(BagSize.MEDIUM);
        IndexOutOfBoundsException exception = assertThrows(
                IndexOutOfBoundsException.class, () ->
                        storage.save(bag, SlotSize.BIG)
        );
        assertEquals("Insufficient capacity", exception.getMessage());
    }

    @Test
    void should_return_ticket_when_save_medium_bag_to_medium_slot() {
        Storage storage = new Storage(0, 1, 0);
        Bag bag = new Bag(BagSize.MEDIUM);
        Ticket ticket = storage.save(bag, SlotSize.MEDIUM);
        assertNotNull(ticket);
    }
}
